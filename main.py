import datetime
import random
import requests 

from flask import Flask, request, Response, json

from utils import parse_length, gen_password_length, gen_current_time

app = Flask(__name__)


@app.route('/')
def hello():
    return('Hello')


@app.route('/now')
def get_current_time():
    return str(gen_current_time())


@app.route('/password')
def get_random():
    try:
        length = parse_length(request, default=10)
    except Exception as ex:
        return Response(str(ex), status=400)

    return gen_password_length(length)


@app.route('/bitcoin')
def get_bitcoin_rate():
    response = requests.get(url='https://bitpay.com/api/rates')
    
    rates = json.loads(response.text)
    for rate in rates:
        if rate['code']== 'USD':
            return str(rate['rate'])
    return 'N/A'


if __name__ == '__main__':

    app.run(host='localhost', port=8003, debug=True)
