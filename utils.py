def parse_length(request, default = 10):
    length = request.args.get('length', str(default))
    if not length.isnumeric():
        raise ValueError('ERROR')
    length = int(length)
    
    if not 3 < length <100:
        raise ValueError('Range Error')
    
    return length


def gen_password_length():
    result=''.join([str(random.randint(0,9)) for i in range(length)])
    return result

def gen_current_time():
    return datetime.datetime.now() # Flask работает только со строками
